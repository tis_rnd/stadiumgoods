class MainController < ApplicationController
  def feed
    render json: Utils::FeedResource.get_data.to_json
  end
end
