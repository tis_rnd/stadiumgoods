require 'concurrent-edge'

module Utils
  class FeedResource
    SLEEP_TIME = 5
    SOCIAL_NETWORKS = {'twitter':'https://takehome.io/twitter', 'facebook':'https://takehome.io/facebook', 'instagram':'https://takehome.io/instagram'}

    def self.get_data
      res = {}
      SOCIAL_NETWORKS.each do |network, url| res[network] = Rails.cache.read(network) end
      res
    end

    def self.initialize
      # init cache storage
      SOCIAL_NETWORKS.each do |network, url|
        Rails.cache.write(network, [])
      end

      channel = Concurrent::Channel
      # start loops
      SOCIAL_NETWORKS.each do |network, url|
        # async
        channel.go do
          loop do
            begin
              data = monitoring url, network
              p data
              Rails.cache.write(network, data) if data
            rescue => e
              p e
            end
            sleep SLEEP_TIME
          end
        end
      end
    end

    def self.monitoring url, network
      res = Net::HTTP.get URI.parse(url)
      if res and res.present?
        res = JSON.parse res
        case network.to_s
          when 'twitter'
            res = res.map do |item| item['tweet'] end
          when 'facebook'
            res = res.map do |item| item['status'] end
          when 'instagram'
            res = res.map do |item| item['photo'] end
          else
        end
      end
    end
  end
end
